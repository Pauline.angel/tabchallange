const Beer = require('../models/beer.model');
const BeerType = require('../models/beer_type.model');

var beerTruck = [];
var kegIdController = 0;


//Some constants for messages
const error404Keg = "{ errorMsg: 'Beer not found on truck' }";


const defineBeerType = function (beerTypeName) {
    console.log("Indo criar o novo tipo de classe de cerveja:" + beerTypeName);
    var newBeerType = new BeerType();
    if ("Pilsner" == beerTypeName) {
        newBeerType.setTypeName("Pilsner");
        newBeerType.setMinTemp(-4);
        newBeerType.setMaxTemp(-6);
    } else if ("IPA" == beerTypeName) {
        newBeerType.setTypeName("IPA");
        newBeerType.setMinTemp(-5);
        newBeerType.setMaxTemp(-6);
    } else if ("Lager" == beerTypeName) {
        newBeerType._typeName = "Lager";
        newBeerType._minTemp = -4;
        newBeerType._maxTemp = -7;
    } else if ("Stout" == beerTypeName) {
        newBeerType.setTypeName("Stout");
        newBeerType.setMinTemp(-6);
        newBeerType.setMaxTemp(-8);
    } else if ("Wheat beer" == beerTypeName) {
        newBeerType.setTypeName("Wheat beer");
        newBeerType.setMinTemp(-3);
        newBeerType.setMaxTemp(-5);
    } else if ("Pale Ale" == beerTypeName) {
        newBeerType.setTypeName("Pale Ale");
        newBeerType.setMinTemp(-4);
        newBeerType.setMaxTemp(-6);
    } else {
        newBeerType.setTypeName("Unknown");
    }
    return newBeerType;
}

exports.addKeg = function (req, res, next) {
    var kegData = {beerType, beerTemp} = req.body;
    var beer = new Beer();
    beer.setBeerType(defineBeerType(kegData.beerType));
    beer.setTemp(kegData.beerTemp);
    beer.setId(kegIdController);

    console.log("creating a new beer with id:" + kegIdController);

    beerTruck.push(beer);
    kegIdController++;

    res.send(beer.toJson());
    return;
};

exports.getKeg = function (req, res, next) {
    const kegId = req.params.id;
    console.log("searching for a keg with id " + kegId);
    for ( var i = 0; i < beerTruck.length; i++) {
        var beer = beerTruck[i];
        console.log("Comparing " + kegId + " with " + beer.getId() + " from the arrey pos " + i);
        if (kegId == beer.getId()) {
            console.log("Beer found returning");
            res.send(beer.toJson());
            return;
        }
    }
    res.send(error404Keg);
    return;
};

exports.updateKeg = function (req, res, next) {
    const kegData = {beerType, beerTemp} = req.body;
    for ( var i = 0; i < beerTruck.length; i++) {
        var beer = beerTruck[i];
        if (kegId == beer.getId()) {
            beerTruck[i] = kegData;
            res.send(beer.toJson());
            return;
        }
    }
    res.send(error404Keg);
    return;
};

exports.deleteKeg = function (req, res, next) {
    const kegId = req.params.id;
    for ( var i = 0; i < beerTruck.length; i++) {
        var beer = beerTruck[i];
        if (kegId == beer.getId()) {
            beerTruck[i] = null;
            res.send(beer.toJson());
            return;
        }
    }
    res.send(error404Keg);
    return;
}