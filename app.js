//base imports
const express = require('express');
const bodyParser =  require('body-parser');

//Routes Imports
const breweri = require('./routes/breweri.route');

//App variable
const app = express();


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use('/breweri', breweri);


let port = 8001;

app.listen(port, function(){
    console.log("Breweri up and runnnig ("+port+").");
});